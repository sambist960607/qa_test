import requests
import sqlite3


def names_id():
    """ Получение id и name из api /api/v3/directory/languages'

    :return: array
    """
    response = requests.get('https://work-api.adtdev.kz/api/v3/directory/languages')
    json = response.json()
    data = json['data']
    names = []

    for res in data:
        id = res['id']
        name = res['name']
        a = (id, name)
        b = tuple(a)
        names.append(b)

    return names


def table(names):
    con = sqlite3.connect('mydatabase.db')
    cur = con.cursor()

    cur.execute("""CREATE TABLE IF NOT EXISTS languages(id INT PRIMARY KEY,languages TEXT);""")  # Создание таблицы
    con.commit()

    cur.executemany("INSERT INTO languages VALUES(?, ?)", names_id())
    print('hello')
    con.commit()

    # cur.execute("DELETE FROM languages",) #Удаление всех значений из таблицы
    # con.commit()


if __name__ == '__main__':
    names = names_id()
    print(names)
    table(names)


