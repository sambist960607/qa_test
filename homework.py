def homework():
    '''
    Задание "Лишний символ" каждый символ из строки "a" сравнивается с каждым символом из списка "arr" и выводит символ которого нет в списке
    :return: homework = list
    '''

    a = 'スーパーハカー'
    b = 'スーパーハッカー'
    arr = []
    ar = []

    for i in a:
        arr.append(i)

    for j in b:
        if j in arr:
           arr.remove(j)
        else:
            ar.append(j)

    print(ar)


if __name__ == '__main__':
    homework()