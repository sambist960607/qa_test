import sqlite3


def tz_sql():
    university_list = [(1, 'Казахская национальная академия искусств имени Т. К. Жургенова', 290),
                    (2, 'Казахская национальная консерватория имени Курмангазы', 300),
                    (3, 'Казахская национальная академия хореографии', 350),
                    (4, 'Казахский национальный аграрный университет', 100),
                    (5, 'Казахский национальный медицинский университет имени С. Д. Асфендиярова', 150),
                    (6, 'Казахский национальный педагогический университет имени Абая', 400),
                    (7, 'Казахский национальный технический университет имени К. И. Сатпаева', 299),
                    (8, 'Казахский национальный университет имени аль-Фараби', 301),
                    (9, 'Казахский национальный университет искусств', 120),
                    (10, 'Атырауский институт нефти и газа', 500)]

    con = sqlite3.connect('table.db')
    cur = con.cursor()
    print("Подключен к SQLite")

    #cur.execute('''CREATE TABLE IF NOT EXISTS iniversityes(id INT PRIMARY KEY,university TEXT, rating INT)''')

    # cur.executemany("INSERT INTO iniversityes VALUES(?, ?, ?);", university_list)
    # con.commit()

    cur.execute('SELECT id, university, rating '
                'FROM iniversityes '
                'WHERE rating > 300')

    rows = cur.fetchall()
    for row in rows:
        print(f'ID: {row[0]},', f'Университет: {row[1]}, ' f'Рейтинг: {row[2]}.')
    print()


def tz_sqlite():
    students_list = [(29, 'Катя', 'Иванова', '3й курс', 105000, 'Воронеж', 1996),
                     (30, 'Настя', 'Петрова', '2й курс', 78000, 'Алматы', 1992),
                     (31, 'Руслан', 'Саловатов', '1й курс', 65500, 'Воронеж', 1995),
                     (32, 'Надя', 'Виноградова', '5й курс', 101000, 'Алматы', 1990),
                     (33, 'Зарина', 'Саламова', '1й курс', 100000, 'Воронеж', 1996),
                     (34, 'Анеля', 'Исламова', '5й курс', 80000, 'Караганда', 1994),
                     (35, 'Ильнур', 'Керимов', '3й курс', 120000, 'Воронеж', 1994),
                     (36, 'Нурлан', 'Саматов', '2й курс', 50000, 'Воронеж', 1997)]

    con = sqlite3.connect('table.db')
    cur = con.cursor()
    print("Подключен к SQLite")

    #cur.execute('''CREATE TABLE IF NOT EXISTS students(id INT PRIMARY KEY, first_name TEXT, surname TEXT, kurs TEXT, stependy INT, town TEXT, year_of_birth INT)''')

    #cur.executemany("INSERT INTO students VALUES(?, ?, ?, ?, ?, ?, ?);", students_list)
    #con.commit()

    cur.execute("SELECT id, first_name, surname, kurs, town, stependy "
                "FROM students "
                "WHERE town = 'Воронеж'"
                "   AND stependy >= 100000;")

    rows = cur.fetchall()

    for row in rows:
        print(f'ID: {row[0]},', f'Имя: {row[1]},', f'Фамилия: {row[2]},', f'Курс: {row[3]},', f'Город: {row[4]},', f'Степендия: {row[5]}.')
    print()


def tz_sql_two():
    con = sqlite3.connect('table.db')
    cur = con.cursor()
    print("Подключен к SQLite")

    #cur.execute("SELECT first_name, surname, year_of_birth FROM students")

    cur.execute("SELECT first_name ||' '|| surname ||' '||'родился в '|| year_of_birth ||' '||'году'"
                " FROM students ")

    rows = cur.fetchall()

    for row in rows:
        print(row)
    print()


def tz_sql_three():
    students_list = [(29, 'Катя', 'Иванова', '3й курс', 105000, 'Воронеж', 1996, 'математика', 5, 20, 1),
                     (30, 'Настя', 'Петрова', '2й курс', 78000, 'Алматы', 1992, 'математика', 2, 21, 3),
                     (31, 'Руслан', 'Саловатов', '1й курс', 65500, 'Воронеж', 1995, 'математика', 4, 19, 2),
                     (32, 'Надя', 'Виноградова', '5й курс', 101000, 'Алматы', 1990, 'математика', 3, 20, 1),
                     (33, 'Зарина', 'Саламова', '1й курс', 100000, 'Воронеж', 1996, 'математика', 5, 25, 4),
                     (34, 'Анеля', 'Исламова', '5й курс', 80000, 'Караганда', 1994, 'математика', 4, 10, 1),
                     (35, 'Ильнур', 'Керимов', '3й курс', 120000, 'Воронеж', 1994, 'математика', 2, 30, 5),
                     (36, 'Нурлан', 'Саматов', '2й курс', 50000, 'Воронеж', 1997, 'математика', 4, 20, 1)]

    con = sqlite3.connect('table.db')
    cur = con.cursor()
    print("Подключен к SQLite")

    # cur.executemany('INSERT INTO students_university VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);', students_list)
    # con.commit()

    cur.execute("SELECT COUNT (first_name) FROM students_university WHERE identifier = 20")

    ar = cur.fetchone()

    print(f'Кол-во студентов сдавших экзамен на 20 баллов: {ar}')


def tz_sql_four():
    arr_list = [(1, 'Нурлан', 5, 4, 2, 4),
                (2, 'Нуриддин', 4, 4, 3, 2),
                (3, 'Зарина', 5, 4, 5, 5),
                (4, 'Катя', 4, 3, 3, 3),
                (5, 'Руслан', 3, 4, 5, 3),
                (6, 'Рустам', 3, 2, 3, 2),
                (7, 'Коля', 4, 5, 2, 2),
                (8, 'Анеля', 3, 2, 3, 3)]


    con = sqlite3.connect('table.db')
    cur = con.cursor()
    print("Подключен к SQLite")

    #cur.execute('''CREATE TABLE IF NOT EXISTS exam_grades(id INT PRIMARY KEY, student_name TEXT, math_score INT, music_score INT, algebra_score INT, geography_score INT)''')
    #con.commit()

    # cur.executemany('INSERT INTO exam_grades VALUES (?, ?, ?, ?, ?, ?);', arr_list)
    # con.commit()

    cur.execute('SELECT id, student_name, MAX(math_score, music_score, algebra_score, geography_score) FROM exam_grades')

    arr = cur.fetchall()

    for array in arr:
        print(f'ID: {array[0]}, Имя: {array[1]}, Максимальная оценка: {array[2]}' )
    print()


def tz_sql_four_part_two():
    arr_list = [(1, 5, 4, 2, 4),
                (2, 4, 4, 3, 2),
                (3, 5, 4, 5, 5),
                (4, 4, 3, 3, 3),
                (5, 3, 4, 5, 3),
                (6, 3, 2, 3, 2),
                (7, 4, 5, 2, 2),
                (8, 3, 2, 3, 3)]

    con = sqlite3.connect('table.db')
    cur = con.cursor()
    print("Подключен к SQLite")

    # cur.execute('''CREATE TABLE IF NOT EXISTS estimates(id INT PRIMARY KEY, mathematics_first_quarter INT,mathematics_second_quarter INT, mathematics_third_quarter INT, mathematics_fourth_quarter INT)''')
    # con.commit()

    # cur.executemany('INSERT INTO estimates VALUES(?, ?, ?, ?, ?);', arr_list)
    # con.commit()

    cur.execute('SELECT exam_grades.id, exam_grades.student_name, MAX(mathematics_first_quarter, mathematics_second_quarter, mathematics_third_quarter, mathematics_fourth_quarter) '
                'FROM exam_grades, estimates '
                'WHERE exam_grades.id = estimates.id')

    arr = cur.fetchall()

    for array in arr:
        print(f'ID: {array[0]}, Имя студента: {array[1]}, Высшая оценка за 4 четверти по математике: {array[2]}  ')
    print()


def tz_sql_five():
    con = sqlite3.connect('table.db')
    cur = con.cursor()
    print("Подключен к SQLite")

    cur.execute(
        'SELECT id, student_name, MIN(math_score, music_score, algebra_score, geography_score) FROM exam_grades')

    arr = cur.fetchall()

    for array in arr:
        print(f'ID: {array[0]}, Имя: {array[1]}, Минимальная оценка: {array[2]}')
    print()


def tz_sql_five_part_two():
    con = sqlite3.connect('table.db')
    cur = con.cursor()
    print("Подключен к SQLite")

    cur.execute(
        'SELECT exam_grades.id, exam_grades.student_name, MIN(mathematics_first_quarter, mathematics_second_quarter, mathematics_third_quarter, mathematics_fourth_quarter) '
        'FROM exam_grades, estimates '
        'WHERE exam_grades.id = estimates.id')

    arr = cur.fetchall()

    for array in arr:
        print(f'ID: {array[0]}, Имя студента: {array[1]}, Низшая оценка за 4 четверти по математике: {array[2]}  ')
    print()


def tz_sql_six():
    name_list = [(1, 'Руслан', 'Ианов'),
                 (2, 'Рустам', 'Ибеков'),
                 (3, 'Алина', 'Иванова'),
                 (4, 'Зарина', 'Тупак'),
                 (5, 'Константин', 'Иракли'),
                 (6, 'Дамир', 'Баранов'),
                 (7, 'Соня', 'Исаева'),
                 (8, 'Максим', 'Илюхин')]

    con = sqlite3.connect('table.db')
    cur = con.cursor()
    print("Подключен к SQLite")

    # cur.execute('''CREATE TABLE IF NOT EXISTS students_list(id INT PRIMARY KEY, name TEXT, curname TEXT) ''')
    # con.commit()

    # cur.executemany('INSERT INTO students_list VALUES(?, ?, ?);', name_list)
    # con.commit()

    cur.execute("SELECT curname FROM students_list WHERE curname LIKE 'И%' ORDER BY curname")

    name_list = cur.fetchall()

    for name in name_list:
        print(name)


def tz_sql_seven():
    subject_list = [(1, 'математика', 1, 2, 3, 4),
                    (2, 'история', 1, 2, 0, 0),
                    (3, 'литература', 1, 2, 3, 0),
                    (4, 'история мира', 0, 0, 3, 4),
                    (5, 'русский язык', 1, 0, 0, 0),
                    (6, 'английский язык', 0, 0, 0, 4),
                    (7, 'алгебра', 1, 2, 3, 4),
                    (8, 'геометрия', 0, 2, 0, 0)]

    con = sqlite3.connect('table.db')
    cur = con.cursor()
    print("Подключен к SQLite")

    # cur.execute('CREATE TABLE IF NOT EXISTS academic_subjects(id INT PRIMARY KEY, subject TEXT, semester_one INT, semester_two INT, semester_thre INT, semester_four INT  )')

    # cur.executemany('INSERT INTO academic_subjects VALUES(?, ?, ?, ?, ?, ?);', subject_list)
    # con.commit()

    cur.execute('SELECT subject, MAX(semester_one, semester_two, semester_thre, semester_four) FROM academic_subjects')

    arr = cur.fetchall()

    for array in arr:
        print(f'Последний семестр преподавания предмета: {array[0]}, семестр: {array[1]} ')


if __name__ == '__main__':
    tz_sql()
    tz_sqlite()
    tz_sql_two()
    tz_sql_three()
    tz_sql_four()
    tz_sql_four_part_two()
    tz_sql_five()
    tz_sql_five_part_two()
    tz_sql_six()
    tz_sql_seven()


